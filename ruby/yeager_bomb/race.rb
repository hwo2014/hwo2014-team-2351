class YeagerBomb::Race
  attr_accessor :frames, :cars, :track

  def initialize(options = {})
    @frames = YeagerBomb::Frames.new

    self
  end

  def set_track(track_data)
    @track = YeagerBomb::Track.new(track_data)
  end
  
  def set_cars(cars_data)
    @cars = []
    cars_data.each do |car_data|
      car      = YeagerBomb::Car.new(car_data)
      car.race = self

      @cars << car
    end

    @cars
  end

  def current_frame
    @frames.last
  end

  def previous_frame
    @frames[-2]
  end

  def current_tick
    current_frame.tick
  end
end
