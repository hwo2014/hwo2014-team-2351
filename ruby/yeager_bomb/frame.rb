class YeagerBomb::Frame
  attr_accessor :tick, :positions, :last_frame

  def initialize(options)
    @tick       = options[:tick]
    @last_frame = options[:last_frame]
    @positions  = options[:positions] || []

    self
  end

  def position_for(car_name)
    @positions.detect do |position|
      position["id"]["name"] == car_name
    end
  end
end
