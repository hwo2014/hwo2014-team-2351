class YeagerBomb::Track
  attr_accessor :pieces

  def initialize(options)
    @id     = options["id"]
    @name   = options["name"]
    @pieces = options["pieces"].each_with_index.map {|p, i| YeagerBomb::Piece.new(p.merge({"index" => i}))}
    @lanes  = options["lanes"]
  end

  def total
    @pieces.map(&:length).inject(&:+)
  end

  def distance_up_to(piece_index)
    index_up_to = piece_index - 1
    if index_up_to < 0
      0
    else
      @pieces[0..index_up_to].map(&:length).inject(&:+)
    end
  end

  def piece_at(piece_index)
    @pieces[piece_index.to_i]
  end
end

=begin
"track"=>{"id"=>"keimola", "name"=>"Keimola", "pieces"=>[{"length"=>100.0}, {"length"=>100.0}, {"length"=>100.0}, {"length"=>100.0, "switch"=>true}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>200, "angle"=>22.5, "switch"=>true}, {"length"=>100.0}, {"length"=>100.0}, {"radius"=>200, "angle"=>-22.5}, {"length"=>100.0}, {"length"=>100.0, "switch"=>true}, {"radius"=>100, "angle"=>-45.0}, {"radius"=>100, "angle"=>-45.0}, {"radius"=>100, "angle"=>-45.0}, {"radius"=>100, "angle"=>-45.0}, {"length"=>100.0, "switch"=>true}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>200, "angle"=>22.5}, {"radius"=>200, "angle"=>-22.5}, {"length"=>100.0, "switch"=>true}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"length"=>62.0}, {"radius"=>100, "angle"=>-45.0, "switch"=>true}, {"radius"=>100, "angle"=>-45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"length"=>100.0, "switch"=>true}, {"length"=>100.0}, {"length"=>100.0}, {"length"=>100.0}, {"length"=>90.0}], "lanes"=>[{"distanceFromCenter"=>-10, "index"=>0}, {"distanceFromCenter"=>10, "index"=>1}], "startingPoint"=>{"position"=>{"x"=>-300.0, "y"=>-44.0}, "angle"=>90.0}}
=end
