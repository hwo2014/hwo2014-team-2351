class YeagerBomb::Car
  attr_accessor :id, :name, :color, :race

  def initialize(options)
    @id = options["id"]

    @name  = @id["name"]
    @color = @id["color"]
  end

  def set_race(race)
    @race = race
  end

  def current_frame
    @race.current_frame
  end

  def current_piece
    @race.track.piece_at(position["piecePosition"]["pieceIndex"])
  end

  def current_angle
    position["angle"]
  end

  def position
    @race.current_frame.position_for(@name)
  end

  def distance_travelled
    distance_at_frame(current_frame)
  end

  def current_velocity
    velocity_at_frame(current_frame)
  end

  def current_acceleration
    acceleration_at_frame(current_frame)
  end

private
  def distance_at_frame(frame)
    if frame.nil? || frame.position_for(@name).nil?
      0
    else
      car_position = frame.position_for(@name)
      piece_position = car_position["piecePosition"]

      laps     = (car_position["lap"].to_i * @race.track.total).to_f
      pieces   = @race.track.distance_up_to(piece_position["pieceIndex"].to_i).to_f
      in_piece = piece_position["inPieceDistance"].to_f

      laps + pieces + in_piece
    end
  end

  def velocity_at_frame(frame)
    if frame.nil?
      0
    else
      prev_frame = frame.last_frame

      d = frame.tick - prev_frame.tick
      return 0 if d == 0

      (distance_at_frame(frame) - distance_at_frame(prev_frame)) / d
    end
  end

  def acceleration_at_frame(frame)
    if frame.nil?
      0
    else
      prev_frame = frame.last_frame
      
      d = frame.tick - prev_frame.tick
      return 0 if d == 0

      (velocity_at_frame(frame) - velocity_at_frame(prev_frame)) / d
    end
  end
end

# {"id"=>{"name"=>"Yeager Bomb", "color"=>"red"}, "dimensions"=>{"length"=>40.0, "width"=>20.0, "guideFlagPosition"=>10.0}}
