class YeagerBomb::Piece
  attr_accessor :radius, :angle, :switch, :index

  def initialize(options)
    @index  = options["index"].to_i
    @length = options["length"].to_f
    @switch = options["switch"]
    @radius = options["radius"] ? options["radius"].to_f : nil
    @angle  = options["angle"].to_f
  end

  def straight?
    @radius.nil? || @radius == 0.0
  end
  
  def curve?
    !straight?
  end

  def length
    if curve?
      (@radius * (Math::PI / 180) * @angle).to_f.abs
    else
      @length
    end
  end
end
