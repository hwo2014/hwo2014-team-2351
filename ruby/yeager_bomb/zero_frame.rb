class YeagerBomb::Frame; end

class YeagerBomb::ZeroFrame < YeagerBomb::Frame
  def initialize
    super(
      tick: 0,
      positions: []
    )
  end
end
