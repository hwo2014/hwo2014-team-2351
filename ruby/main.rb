require 'json'
require 'socket'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

require './yeager_bomb'

class YeagerBomb::Racer
  attr_accessor :race

  def initialize(server_host, server_port, bot_name, bot_key)
    @race = YeagerBomb::Race.new
    @tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key)
  end

  private

  def play(bot_name, bot_key)
    @tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server
  end

  def get_piece(piece_index)
    @pieces[piece_index]
  end

  def get_next_piece(currentIndex)
    @pieces[(currentIndex + 1) % @pieces.length]
  end



  def curved?(piece_index, current_pos)
    piece = get_piece(piece_index)
    piece["angle"] != nil
  end

  def close_to_curved?(piece_index, current_pos)
    return true if curved?(piece_index, current_pos)


    piece = get_piece(piece_index)
    next_piece = get_next_piece(piece_index)

    next_piece["angle"] != nil &&
      piece["length"] - current_pos < 40
  end

  # we might not be position 0, so fix that later
  def get_position(message)
    message["data"][0]["piecePosition"]["inPieceDistance"] || 0
  end

  def get_tick(message)
    message["gameTick"] || 0
  end

  def get_piece_index(message)
    message["data"][0]["piecePosition"]["pieceIndex"]
  end

  # depending on which lane you're in, this'd be slightly different
  def get_length(piece)
    if piece["length"]
      piece["length"]
    else
      radius = piece["radius"]
      angle  = piece["angle"]
      angle * (Math::PI/180) * radius
    end
  end


  def get_velocity
    if @piece_index == @last_piece_index
      delta_x = (@position - @last_position)/(@tick - @last_tick)
    else
      last_piece = get_piece(@last_piece_index)
      last_length = get_length(last_piece)

      delta_x = @position - (last_length - @last_position)
    end

    delta_x/(@tick - @last_tick)
  end

  def get_car_throttle(message)
    @last_position    = @position.to_f
    @last_tick        = @tick.to_i
    @last_piece_index = @piece_index.to_i
    @last_velocity    = @velocity.to_f

    @position    = get_position(message)
    @tick        = get_tick(message)
    @piece_index = get_piece_index(message)

    @velocity = get_velocity
    @acceleration = (@velocity - @last_velocity)/(@tick - @last_tick)

    puts [@last_position, @last_tick, @last_piece_index].join(" :: ")
    puts [@position, @tick, @piece_index, @velocity, @acceleration].join(" :: ")
    puts "---"
    # location = data[0]["piecePosition"]
    # piece_index = location["pieceIndex"]

    # current_piece = @pieces[piece_index]
    # current_pos   = location["inPieceDistance"]

    # puts current_piece

    # if curved?(piece_index, current_pos)
    #   accel = 2.5
    # elsif close_to_curved?(piece_index, current_pos)
    #   accel = 2.5
    # end
    # if curved?(piece_index, current_pos) ||
    #   close_to_curved?(piece_index, current_pos)

    #   accel = 0.5
    # else
    #   accel = 1.0
    # end

    # puts "piece: #{@pieces[location["pieceIndex"]]}, pos: #{location["inPieceDistance"]}"
    # puts "throttle: #{accel}"
    accel = 1.0 #@tick.even? ? 0.25 : 0.75
    accel = 0.7 if @tick > 60
    @statistics.push({
      throttle: accel,
      position: @position,
      tick: @tick,
      piece_index: @piece_index,
      velocity: @velocity,
      acceleration: @acceleraton,
      angle: message["data"][0]["angle"]
    })

    @tcp.puts throttle_message(accel, @tick)
  end

  def react_to_messages_from_server
    while json = @tcp.gets
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      puts message.inspect
      case msgType
        when 'carPositions'
          @tcp.puts get_car_throttle(message)

          @race.frames << {tick: get_tick(message), positions: msgData}

          puts "CAR AT #{@race.current_tick} (TOTAL: #{@race.track.total}):"
          @race.cars.each do |car|
            puts "\t#{car.name} on #{car.current_piece.length} - #{car.current_piece.curve?}\t#{car.distance_travelled}\t#{car.current_velocity}\t#{car.current_acceleration}"
            if ENV['WRITE'] == '1'
              File.open("test-run.tsv", "a") do |f|
                f.puts "#{@race.current_tick}\t#{car.current_piece.index}\t#{car.current_piece.length}\t#{car.current_piece.radius}\t#{car.current_piece.angle}\t#{car.current_velocity}\t#{car.current_acceleration}\t#{car.current_angle}"
              end
            end
          end
        else
          case msgType
            when 'join'
              puts 'Joined'
              @tcp.puts ping_message
            when 'gameInit'
              @pieces = msgData["race"]["track"]["pieces"]
              @statistics = []

              @race.set_track(msgData["race"]["track"])
              @race.set_cars(msgData["race"]["cars"])

            when 'gameStart'
              puts 'Race started'
            when 'crash'
              puts 'Someone crashed'
            when 'gameEnd'
              record_stats(@statistics)
              puts 'Race ended'
            when 'error'
              puts "ERROR: #{msgData}"
          end
          puts "Got #{msgType}"
          puts msgData
          #@tcp.puts ping_message
      end
    end
  end

  def record_stats(stats)
    return if ENV["GRUFF"].nil?
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle, tick = nil)
    if tick.nil?
      make_msg("throttle", throttle)
    else
      make_msg("throttle", throttle, tick)
    end
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data, tick = nil)
    if tick.nil?
      JSON.generate({:msgType => msgType, :data => data})
    else
      JSON.generate({:msgType => msgType, :data => data, "gameTick" => tick})
    end
  end
end

YeagerBomb::Racer.new(server_host, server_port, bot_name, bot_key)
