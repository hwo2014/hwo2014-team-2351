class YeagerBomb::Frames < Array
  def initialize
    super(1, YeagerBomb::ZeroFrame.new)
  end

  def <<(frame_options)
    last_frame = last

    push(YeagerBomb::Frame.new(frame_options.merge(last_frame: last_frame)))
  end
end
